$(function() {
    setTimeout(function() {
      $('.text-animation').removeClass('hidden');
    }, 500);
  }());

//   cart

$(".hover").mouseleave(
    function () {
      $(this).removeClass("hover");
    }
  );

  
//   

document.addEventListener('mousemove', function(e) {
    var w = window.innerWidth;
    var cw = w/2;
    var h = window.innerHeight;
    var ch = h/2;
    
    document.documentElement.style.setProperty('--x-offset', (e.clientX-cw)/w);
    document.documentElement.style.setProperty('--y-offset', (e.clientY-ch)/h);
  });

  //Cursor

  const cursor = document.querySelector('.cursor');

document.addEventListener('mousemove', e => {
    cursor.setAttribute('style', 'top:'+(e.pageY - 20)+"px; left:"+(e.pageX - 20)+"px;")
})

document.addEventListener('click', ()=>{
    cursor.classList.add('expand');

    setTimeout(()=>{
        cursor.classList.remove("expand");
    }, 500);
})

// H1 effect
// $(window).load(function(){
//     $('.effect').each(function(){
//       $(this).addClass('active');
//     })
//     $('.fade').addClass('active');
//   });
  
//   $(document).ready(function(){
    
//     $('.cta').click(function(){
//       $('html, body').animate({
//         scrollTop: $("#anchor").offset().top
//       }, 1000);
//     });
    
//     $('.first').bind('mousewheel', function(e){
//       return false;
//     });
  
//   });
var Emblem = {
    init: function(el, str) {
      var element = document.querySelector(el);
      var text = str ? str : element.innerHTML;
      element.innerHTML = '';
      for (var i = 0; i < text.length; i++) {
        var letter = text[i];
        var span = document.createElement('span');
        var node = document.createTextNode(letter);
        var r = (360/text.length)*(i);
        var x = (Math.PI/text.length).toFixed(0) * (i);
        var y = (Math.PI/text.length).toFixed(0) * (i);
        span.appendChild(node);
        span.style.webkitTransform = 'rotateZ('+r+'deg) translate3d('+x+'px,'+y+'px,0)';
        span.style.transform = 'rotateZ('+r+'deg) translate3d('+x+'px,'+y+'px,0)';
        element.appendChild(span);
      }
    }
  };
  
  Emblem.init('.emblem');